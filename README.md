# c64exec

Did you ever want to write a .gitlab-ci.yml job in BASIC? c64exec is a custom runner for GitLab that almost makes it like you have a fleet of Commodore 64s that you can run builds on. Why? Good question, but it seemed fun to try. The basic operation is as follows:

The Runner first does its normal thing where it fetches the repo locally when it receives a job to process. The `build_script` function is then overridden using the [Custom Executor](https://docs.gitlab.com/runner/executors/custom.html) to do the following:

1. Take the contents of the git repo and generate a Commodore compatible (.d64) disk image
1. Extract the BASIC code from the `script` section of the job and save it into the disk image as a PRG (program) file
1. Start up the emulator with the disk image mounted and set to autorun the .gitlab-ci.yml program
1. After the emulator completes, unpack the disk image over the local copy of the repo

The example here uses the custom executor to run a [BASIC program](https://gitlab.com/jlenny/c64exec/blob/master/.gitlab-ci.yml) that reads the contents of `infile` and writes it three times to `newfile`. That file is then saved as an artifact to the pipeline. This demonstrates accessing a file in the repo, processing it, and making it available as an artifact to GitLab.

## Notes and Limitations

- Commodore text encoding is often from the point of view of outside the system in all lowercase, even though it is displayed internally as all uppercase. It's easy to make a mistake where you write a command in uppercase in the .gitlab-ci.yml but this is interpreted as special characters inside the emulator.
- Similarly, double check your YAML encoding if you're running into trouble.
- If the program halts for whatever reason, the emulator will remain running locally. This is nice for playing around and debugging, but if you ever tried to do anything serious with this it would be annoying.
- If your repository gets larger than a c64 disk image supports, bad things (tm) will happen.
- [c1541](http://vice-emu.sourceforge.net/vice_13.html) is a very handy tool for debugging things so you can see what's on disk.

## Installation Steps

1. You will need VICE on your Runner system: http://vice-emu.sourceforge.net/. The executable is expected in /usr/local/bin.
1. You'll need to [install the Runner](https://docs.gitlab.com/runner/install/) and register it per the instructions at that link. On a current Mac you may need to follow the instructions at https://dani.gg/467-add-usr-local-bin-to-the-path-variable-on-mac-os/ to add /usr/local/bin to the system path.
1. After registering, modify the Runner configuration per the example config.toml below (details on where to find the config.toml and other settings available there can be found at https://docs.gitlab.com/runner/configuration/advanced-configuration.html). 

## Todos

In the future it may be possible to make this work with a container image that has everything and could run on a shared runner.

## Example config.toml

You will need to modify the token and paths below for your local machine/GitLab instance.

- The builds_dir and cache_dir don't need anything special in them to start, but they will be the working directory for the Runner
- The run_exec path should point to the run.sh script inside where you cloned this repository

```toml
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "c64exec"
  url = "https://gitlab.com/"
  token = "YOUR TOKEN"
  executor = "custom"
  builds_dir = "/Users/jyavorska/builds"
  cache_dir = "/Users/jyavorska/cache"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
  [runners.custom]
    run_exec = "/Users/jyavorska/code/c64exec/run.sh"
    graceful_kill_timeout = 200
    force_kill_timeout = 200
```
