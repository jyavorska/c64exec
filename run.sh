#!/usr/bin/env bash

# Unsupported (yet): before_script, after_script

cd $CUSTOM_ENV_CI_PROJECT_DIR

if [ "$2" != "build_script" ]; then
  # this job does not run the script section, just execute it normally
  $1 $2
else
  # parse .gitlab-ci.yml to generate .bas
  ruby -e "require 'yaml'; yaml = YAML.load_file('.gitlab-ci.yml'); puts yaml['"$CUSTOM_ENV_CI_JOB_NAME"']['script']" >gitlabci.bas
  /usr/local/bin/petcat -w2 -o gitlabci -- gitlabci.bas
  rm gitlabci.bas
  rm .gitlab-ci.yml 
  # convert repo into disk image
  # todo - deal with subdirectories
  /usr/local/bin/c1541 -format "runtime,0" d64 $CUSTOM_ENV_CI_JOB_NAME.d64
  ls |grep -v "$CUSTOM_ENV_CI_JOB_NAME.d64" |xargs -n1 /usr/local/bin/c1541 -attach $CUSTOM_ENV_CI_JOB_NAME.d64 -write 
  # run emulator in console mode with disk image mounted and autorun generated .bas
  # todo - figure out how to get some output while running
  /usr/local/bin/x64 -debugcart -8 "$CUSTOM_ENV_CI_JOB_NAME.d64" "$CUSTOM_ENV_CI_JOB_NAME.d64:gitlabci" 
  # unpack updated disk image over filesystem so any changes can be used as artifacts
  /usr/local/bin/c1541 -attach $CUSTOM_ENV_CI_JOB_NAME.d64 -extract
fi

